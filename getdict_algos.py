import sys
import numpy
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import matplotlib.pyplot as plt
plt.rcParams['axes.facecolor'] = 'white'
plt.rcParams['font.size'] = 16
plt.rcParams['figure.facecolor'] = 'white'
print "Processing"
def extractData(fileName, skipEvts=10):
    f = open(fileName, 'r')
    res = {}
    evtset = set([])
    for l in f.readlines()[1:]:
        s, e, a, t, sl, ne = l.split()  #start end algorithm thread slot event
        if a.find('Seq') >= 0 or a == 'Reco':
            continue
        evtset.add(ne)
        s = int(s)
        e = int(e)
        if a not in res:
            res[a] = 0
        res[a] += e-s
    f.close()
    return len(evtset), res

nbevts, data = extractData(sys.argv[1])
#print nbevts, data

# cleanup data, by merging everything below 2%
cdata = {}
s = sum(data.values())
other = 0
for alg in data:
    # if data[alg] < s*0.015:
    #     other += data[alg]
    # else:
    cdata[alg] = data[alg]
# cdata['other'] = other

# compute time spent per event
throughput = 1000000000.0*nbevts/s
print "-----------"
print cdata
import operator
ccdata = sorted(cdata.items(), key=operator.itemgetter(1))
tottime = 0.
datapercent = {}
for timing in ccdata:
    print timing[0]
    print timing[1]
    tottime += float(timing[1])
for algotiming in ccdata:
    datapercent[algotiming[0]] = 100. * float(algotiming[1])/tottime
    print "Algo = "+algotiming[0] + " timing = "+str(100. * float(algotiming[1])/tottime) +" %"
print "----------"
print ccdata
import pickle

# Store data (serialize)
with open('results_algosplit_percent.pickle', 'wb') as handle:
    pickle.dump(datapercent, handle, protocol=pickle.HIGHEST_PROTOCOL)

