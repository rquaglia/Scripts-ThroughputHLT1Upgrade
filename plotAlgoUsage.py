import sys
import numpy
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import matplotlib.pyplot as plt
plt.rcParams['axes.facecolor'] = 'white'
plt.rcParams['font.size'] = 16
plt.rcParams['figure.facecolor'] = 'white'

def extractData(fileName, skipEvts=10):
    f = open(fileName, 'r')
    res = {}
    evtset = set([])
    for l in f.readlines()[1:]:
        s, e, a, t, sl, ne = l.split()  #start end algorithm thread slot event
        if a.find('Seq') >= 0 or a == 'Reco':
            continue
        evtset.add(ne)
        s = int(s)
        e = int(e)
        if a not in res:
            res[a] = 0
        res[a] += e-s
    f.close()
    return len(evtset), res

nbevts, data = extractData(sys.argv[1])
#print nbevts, data

# cleanup data, by merging everything below 2%
cdata = {}
s = sum(data.values())
other = 0
for alg in data:
    if data[alg] < s*0.015:
        other += data[alg]
    else:
        cdata[alg] = data[alg]
cdata['other'] = other

# compute time spent per event
throughput = 1000000000.0*nbevts/s
print "-----------"
print cdata
import operator
ccdata = sorted(cdata.items(), key=operator.itemgetter(1))
print "----------"
print ccdata

i =0
color = []
dataprep = [ "createFTClusters", "PrStoreFTHit", "PrStoreUTHit", "VPCLustering", "createUTLiteClusters", "FetchDataFromFile"," VPClustering","other", "DummyEventTime"]
for val in ccdata:
    print val[0]
    if val[0] in dataprep:
        color.append( "b")
    else:
        color.append( "r")
    i = i+1
print color

# Setup colors
colors = cm.rainbow(numpy.linspace(0., 1., len(cdata.keys())))

plt.axes(aspect=1)


plt.figure(1)
val0 = []
val1 = []
for value in ccdata:
    val0.append( value[0])
    val1.append( value[1])
#plt.pie(cdata.values(), [0.02] * len(cdata.keys()), cdata.keys(), autopct='%1.1f%%', shadow=True, startangle=90, colors=colors)
plt.pie(val1, [0.02] * len(cdata.keys()), val0, autopct='%1.1f%%', shadow=True, startangle=90, colors=colors)
plt.tight_layout()

#plt.title('Timing division among algorithm' % throughput)
plt.title('Time Usage in MiniBrunel')
plt.figure(2)
pos = numpy.arange( len(ccdata ) )+.5
plt.barh(   pos, [val[1]*100.0/s*1.0 for val in ccdata], align = 'center', color = color )
plt.yticks( pos, [val[0] for val in ccdata ])
plt.xlabel( "Timing fractiion within the HLT1 sequence [%]")
plt.autoscale()
#manager = plt.get_current_fig_manager()
#manager.resize(*manager.window.maxsize())
plt.grid()
plt.show()
