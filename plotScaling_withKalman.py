import sys
import numpy
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.patches as patches
import numpy as np



def pretty(d, indent=0):
   for key, value in d.items():
      print('\t' * indent + str(key))
      if isinstance(value, dict):
         pretty(value, indent+1)
      else:
         print('\t' * (indent+1) + str(value))
         
fontsize = 20
fontweight = 'bold'
fontproperties = {'family':'sans-serif','sans-serif':['Helvetica'],'weight' : fontweight, 'size' : fontsize}
plt.rcParams['axes.facecolor'] = 'white'
plt.rcParams['font.size'] = 16
plt.rcParams['figure.facecolor'] = 'white'
plt.style.use('seaborn-whitegrid')
plt.rcParams['font.size'] = 16
from collections import OrderedDict

path_fwd1000_vut800 = "./DefaultTDR_NoKalman_50k_FWD1000_VUT800_"
path_fwd800_vut600 = "./DefaultTDR_NoKalman_50k_FWD800_VUT600_"
path_fwd600_vut400 = "./DefaultTDR_NoKalman_50k_FWD600_VUT400_"
path_fwd400_vut300 = "./DefaultTDR_NoKalman_50k_FWD400_VUT300_"

inputfiles = {  "FWD1000VUT800" :  {"noIP" : path_fwd1000_vut800+"IP0/results_algosplit_percent.pickle" ,
							 		"IP20" : path_fwd1000_vut800+"IP20/results_algosplit_percent.pickle" ,
								    "IP40" : path_fwd1000_vut800+"IP40/results_algosplit_percent.pickle"	,
								    "IP60" : path_fwd1000_vut800+"IP60/results_algosplit_percent.pickle"	,
								    "IP80" : path_fwd1000_vut800+"IP80/results_algosplit_percent.pickle"	,							 
							 		"IP100": path_fwd1000_vut800+"IP100/results_algosplit_percent.pickle"}
		,							 
		"FWD800VUT600" :     {		"noIP" : path_fwd800_vut600+"IP0/results_algosplit_percent.pickle" ,
							 		"IP20" : path_fwd800_vut600+"IP20/results_algosplit_percent.pickle" ,
								    "IP40" : path_fwd800_vut600+"IP40/results_algosplit_percent.pickle"	,
								    "IP60" : path_fwd800_vut600+"IP60/results_algosplit_percent.pickle"	,
								    "IP80" : path_fwd800_vut600+"IP80/results_algosplit_percent.pickle"	,							 
							 		"IP100": path_fwd800_vut600+"IP100/results_algosplit_percent.pickle"},

		"FWD600VUT400" :     {		"noIP" : path_fwd600_vut400+"IP0/results_algosplit_percent.pickle" ,
							 		"IP20" : path_fwd600_vut400+"IP20/results_algosplit_percent.pickle" ,
								    "IP40" : path_fwd600_vut400+"IP40/results_algosplit_percent.pickle"	,
								    "IP60" : path_fwd600_vut400+"IP60/results_algosplit_percent.pickle"	,
								    "IP80" : path_fwd600_vut400+"IP80/results_algosplit_percent.pickle"	,							 
							 		"IP100": path_fwd600_vut400+"IP100/results_algosplit_percent.pickle"},

		 "FWD400VUT300" :{			"noIP" : path_fwd400_vut300+"IP0/results_algosplit_percent.pickle" ,
							 		"IP20" : path_fwd400_vut300+"IP20/results_algosplit_percent.pickle" ,
								    "IP40" : path_fwd400_vut300+"IP40/results_algosplit_percent.pickle"	,
								    "IP60" : path_fwd400_vut300+"IP60/results_algosplit_percent.pickle"	,
								    "IP80" : path_fwd400_vut300+"IP80/results_algosplit_percent.pickle"	,							 
							 		"IP100": path_fwd400_vut300+"IP100/results_algosplit_percent.pickle"}							 		 

}

data = {  "FWD1000VUT800" :  {"noIP" : 10074 ,
							 "IP20" : 10922 ,
							 "IP40" : 12489	,
							 "IP60" : 13467	,
							 "IP80" : 13710	,							 
							 "IP100": 13872	}
		,							 
		"FWD800VUT600" :     {"noIP" : 7028 ,
							 "IP20" : 9382 ,
							 "IP40" : 11409	,
							 "IP60" : 12609	,
							 "IP80" : 13115	,							 
							 "IP100": 13400	},

		"FWD600VUT400" :     {"noIP" : 6486 ,
							 "IP20" : 7359 ,
							 "IP40" : 9433	,
							 "IP60" : 11025	,
							 "IP80" : 11943	,							 
							 "IP100": 12355},

		 "FWD400VUT300" :    {"noIP" : 4811 ,
							 "IP20" : 5524 ,
							 "IP40" : 7258	,
							 "IP60" : 9196	,
							 "IP80" : 10458	,							 
							 "IP100": 10819	}							 		 

}

colors = cm.brg(numpy.linspace(0., 1., len(data)))
j=0

fig = plt.figure(figsize=(20.841, 10.0), dpi=100)
ax = fig.add_subplot(111)
for TrackingConf, IPConf in sorted(data.items()):
	tt = TrackingConf
	Fwdpt = tt.split("VUT")[0].split("FWD")[1]
	Vutpt = tt.split("VUT")[1]

	print TrackingConf
	IPValues = []
	Throughput = []	
	for IPcutVal, throughput in IPConf.items():
		print "\t "+IPcutVal +" : "+str(throughput)

		if( "noIP" in IPcutVal):
			ip = 0
			IPValues += [0]
			Throughput += [throughput]
		else:
			ip = IPcutVal.split("IP")[1]
			IPValues+=[float(ip)]
			Throughput += [throughput]
	ax.scatter(IPValues,Throughput, color=colors[j], lw=1, label = "min $p^{FWD}_{T}$  "+Fwdpt +" MeV/c, min $p^{VUT}_{T}$ = "+Vutpt+" MeV/c")
	j = j+1
handless, labelss = ax.get_legend_handles_labels()	
ax.legend(handless, labelss, title = "Tracking configurations", loc = "right bottom")
plt.ylabel("Throughput HLT1 [evts/s/node]")
plt.xlabel("IPCut [$\mu$ m]")
plt.title('Throughput scaling for HLT1 sequence')

plt.show()


import pickle

colors = cm.brg(numpy.linspace(0., 1., len(data)))

fig2 = plt.figure(figsize=(20.841, 10.0), dpi=100)
ax2 = fig.add_subplot(111)
#we want a dictionary with 2 lists of [IPCut] [ThroughputAlgorithm]
dataalgo = {}
for TrackingConf, IPConf in sorted(inputfiles.items()):
	tt = TrackingConf
	Fwdpt = tt.split("VUT")[0].split("FWD")[1]
	Vutpt = tt.split("VUT")[1]	

	IPValues = []
	Throughput_Algo = {}
	for IPcutVal, inputfile in IPConf.items():
		dd = pickle.load(open(inputfile, "rb"))
		pretty(dd)
		for alg,fractionthroghputALG in dd.iteritems():
			print alg+"   "+str(fractionthroghputALG)+" %"
			if alg not in dataalgo:
				dataalgo[alg] = {}
			if TrackingConf not in dataalgo[alg]:
				dataalgo[alg][TrackingConf] = {}
			if IPcutVal not in dataalgo[alg][TrackingConf]:
				dataalgo[alg][TrackingConf][IPcutVal] = 0
			print "TrackingConf "+ TrackingConf +" IPConf = "+ IPcutVal
			#throuhgput total = X  t(total) = 1/X t (algo) = fraction * t(total).  throughput(algo) = 1/( fraction * t total ) = trhoguhput total / fraction
			#algo takes 
			dataalgo[alg][TrackingConf][IPcutVal] =  (100./fractionthroghputALG) * data[TrackingConf][IPcutVal]
pretty(dataalgo)





colors = cm.brg(numpy.linspace(0., 1., len(data)))
i = 2

# plt.switch_backend('TkAgg') #TkAgg (instead Qt4Agg)
# print '#1 Backend:',plt.get_backend()
# # mng = plt.get_current_fig_manager()
# # ### works on Ubuntu??? >> did NOT working on windows
# # # mng.resize(*mng.window.maxsize())
# # mng.window.state('zoomed') #works fine on Windows!
# mng = plt.get_current_fig_manager()
# mng.window.state('zoomed') #works fine on Windows!

for algorithm, configresults in dataalgo.items():
	j=0
	fig = plt.figure(i,figsize=(20.841, 10.0), dpi=100)
	ax = fig.add_subplot(111)
	for TrackingConf, IPConf in sorted(configresults.items()):
		tt = TrackingConf
		Fwdpt = tt.split("VUT")[0].split("FWD")[1]
		Vutpt = tt.split("VUT")[1]

		print TrackingConf
		IPValues = []
		Throughput = []	
		for IPcutVal, throughput in IPConf.items():
			print "\t "+IPcutVal +" : "+str(throughput)

			if( "noIP" in IPcutVal):
				ip = 0
				IPValues += [0]
				Throughput += [throughput/1000.]
			else:
				ip = IPcutVal.split("IP")[1]
				IPValues+=[float(ip)]
				Throughput += [throughput/1000.]
		ax.scatter(IPValues,Throughput, color=colors[j], lw=1, label = "min $p^{FWD}_{T}$  "+Fwdpt +" MeV/c, min $p^{VUT}_{T}$ = "+Vutpt+" MeV/c")
		j = j+1
	handless, labelss = ax.get_legend_handles_labels()	
	ax.legend(handless, labelss, title = "Algorithm "+algorithm+ " Tracking configurations", loc = "right bottom")
	plt.ylabel("Throughput [Kevts/s/node]")
	plt.xlabel("IPCut [$\mu$ m]")	
	plt.title('Throughput scaling for '+algorithm)
	# plt.yscale('log')
	plt.grid(True)
	# plt.ylim(0,100000)
	plt.savefig(algorithm+'_throughput.png', bbox_inches='tight',dpi = 100)

	i = i+1

plt.show()

