import sys
import numpy
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.patches as patches

def pretty(d, indent=0):
   for key, value in sorted( d.items(), key= lambda x : x[1]):
      if isinstance(value, dict):
         print('\t' * indent + str(key))
         pretty(value, indent+1)
      else:
         print(str(key)+' \t & ' * (indent+1) + str(value) + ' \\\ ')

times, mems = eval(open(sys.argv[1]).read())
# compute full list of points to display from the raw values
# each point is a couple x, y and lists are indexed by nbThreads

# Extract data from input
if len(times.keys()) > 1:
    print "Not supporting mixed CMTCONFIG, giving up"
    sys.exit(-1)
times = times[times.keys()[0]]

Throughput = 0.
points = {}
pointsNH = []
for nt, nj, useHive in times:
    print(nt) 
    print(nj)
    print(useHive)
    nbEvents, duration = times[(nt,nj,useHive)]
    if useHive:
        if nt not in points:
            points[nt] = []
        print "filling points[nt]"
        points[nt].append((nt*nj, nbEvents/duration))
        print " nt*nj, throughput =  [ "+str(nt*nj)+","+str( nbEvents/duration) +" ]"
        Throughput = nbEvents/duration
    else:
        print "filling points NH"
        pointsNH.append((nt*nj, nbEvents/duration))
        Throughput = nbEvents/duration

import pickle
dd = pickle.load(open(sys.argv[2], "rb"))
dataalgo = {}
for alg,fractionthroghputALG in dd.iteritems():
    print alg+"   "+str(fractionthroghputALG)+" %"
    if alg not in dataalgo:
        dataalgo[alg] = 0.
        dataalgo[alg] =  (100./fractionthroghputALG) * Throughput /1000. 

dataalgo["total"]=Throughput/1000.
import operator
#results = sorted(dataalgo.items(), key= lambda x:x[1])
print "----------------------------------"
#print(results)
pretty(dataalgo)

print "----------------------------------"







